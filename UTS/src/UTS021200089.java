
import java.util.Scanner;


public class UTS021200089 {
    public static void main(String[] args) {
        
        double nilai_tugas,nilai_uts,nilai_uas,nilaiakhir;
        String nhuruf, nama, npm, prodi;
        Scanner inp = new Scanner(System.in);
        
        // Input nilai mahasiswa
        System.out.print(" NPM : ");
        npm=inp.nextLine();
        System.out.print(" Nama : ");
        nama=inp.nextLine();
        System.out.print(" Prodi : ");
        prodi=inp.nextLine();
        System.out.print(" Nilai Tugas : ");
        nilai_tugas=inp.nextDouble();
        System.out.print(" Nilai UTS : ");
        nilai_uts=inp.nextDouble();
        System.out.print(" Nilai UAS : ");
        nilai_uas=inp.nextDouble();
        
        // nilai Tugas = 40%, UTS = 25%, UAS = 35%
        nilaiakhir=(40*nilai_tugas)/100 + (nilai_uts*0.25)+(nilai_uas * 0.35);
        System.out.println(" Nilai Akhir : " + nilaiakhir);
        
        // Cek kondisi untuk mendapatkan nilai huruf -------
        if (nilaiakhir >=85 && nilaiakhir <=100)
        {
        nhuruf="Lulus dengan Grade A";
        }
        else if (nilaiakhir >=70 && nilaiakhir <85)
        {
        nhuruf="Lulus dengan Grade B";
        }
        else if (nilaiakhir >=60 && nilaiakhir < 70)
        {
        nhuruf="Lulus dengan Grade C";
        }
        else if (nilaiakhir >=50 && nilaiakhir <60)
        {
        nhuruf="Tidak Lulus dengan Grade D";
        }
        else 
        {
        nhuruf="Tidak Lulus dengan Grade E";
        }
        System.out.println(" Dinyatakan : " + nhuruf);
   }
}

